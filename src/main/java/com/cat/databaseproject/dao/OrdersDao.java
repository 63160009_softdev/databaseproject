/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cat.databaseproject.dao;

import com.cat.databaseproject.helper.DatabaseHelper;
import com.cat.databaseproject.model.OrderDetail;
import com.cat.databaseproject.model.Orders;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class OrdersDao implements Dao<Orders> {

    @Override
    public Orders get(int id) {
        OrderDetailDao orderDetailDao = new OrderDetailDao();
        Orders order = null;
        String sql = "SELECT * FROM orders WHERE orders_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                order = Orders.fromRS(rs);
                List<OrderDetail> orderDetails = orderDetailDao.getByOrderId(order.getId());
                order.setOrderDetails((ArrayList<OrderDetail>) orderDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return order;
    }

    public List<Orders> getAll() {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM orders";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders order = Orders.fromRS(rs);
                list.add(order);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Orders> getAll(String where, String orderBy) {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM orders where " + where + " ORDER BY " + orderBy;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders order = Orders.fromRS(rs);
                list.add(order);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Orders> getAll(String orderBy) {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM orders  ORDER BY " + orderBy;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders order = Orders.fromRS(rs);
                list.add(order);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Orders save(Orders obj) {

        OrderDetailDao orderDetailDao = new OrderDetailDao();
        String sql = "INSERT INTO orders (orders_total, orders_qty, orders_date)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        DatabaseHelper.beginTransaction();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getTotal());
            stmt.setInt(2, obj.getQty());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(3, sdf.format(obj.getDate()));
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            //Add detail
            obj.setId(id);
            for (OrderDetail od : obj.getOrderDetails()) {
                OrderDetail detail = orderDetailDao.save(od);
                if (detail == null) {
                    DatabaseHelper.endTransactionWithRollback();
                    return null;
                }
            }
            DatabaseHelper.endTransactionWithCommit();
            return get(id);
        } catch (SQLException ex) {
            DatabaseHelper.endTransactionWithRollback();
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Orders update(Orders obj) {
//        String sql = "UPDATE order"
//                + " SET order_login = ?, order_name = ?, order_gender = ?, order_password = ?, order_role = ?"
//                + " WHERE order_id = ?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, obj.getLogin());
//            stmt.setString(2, obj.getName());
//            stmt.setString(3, obj.getGender());
//            stmt.setString(4, obj.getPassword());
//            stmt.setInt(5, obj.getRole());
//            stmt.setInt(6, obj.getId());
////            System.out.println(stmt);
//            int ret = stmt.executeUpdate();
//            System.out.println(ret);
//            return obj;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//            return null;
//        }
        return null;
    }

    @Override
    public int delete(Orders obj) {
        String sql = "DELETE FROM orders WHERE orders_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
