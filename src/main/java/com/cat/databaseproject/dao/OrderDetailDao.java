/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cat.databaseproject.dao;

import com.cat.databaseproject.helper.DatabaseHelper;
import com.cat.databaseproject.model.OrderDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class OrderDetailDao implements Dao<OrderDetail> {

    @Override
    public OrderDetail get(int id) {
        OrderDetail order_detail = null;
        String sql = "SELECT * FROM order_detail WHERE order_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                order_detail = OrderDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return order_detail;
    }

    public List<OrderDetail> getAll() {
        ArrayList<OrderDetail> list = new ArrayList();
        String sql = "SELECT * FROM order_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderDetail order_detail = OrderDetail.fromRS(rs);
                list.add(order_detail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<OrderDetail> getAll(String where, String order) {
        ArrayList<OrderDetail> list = new ArrayList();
        String sql = "SELECT * FROM order_detail where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderDetail order_detail = OrderDetail.fromRS(rs);
                list.add(order_detail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<OrderDetail> getByOrderId(int orderId) {
        return getAll("orders_id = " + orderId, "order_detail_id ASC");
    }
    

    public List<OrderDetail> getAll(String order) {
        ArrayList<OrderDetail> list = new ArrayList();
        String sql = "SELECT * FROM order_detail  ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderDetail order_detail = OrderDetail.fromRS(rs);
                list.add(order_detail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

        @Override
    public OrderDetail save(OrderDetail obj) {

        String sql = "INSERT INTO order_detail (product_id, qty, product_price, product_name, orders_id)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct().getId());
            stmt.setInt(2, obj.getQty());
            stmt.setDouble(3, obj.getProductPrice());
            stmt.setString(4, obj.getProductName());
            stmt.setInt(5, obj.getOrder().getId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public OrderDetail update(OrderDetail obj) {
//        String sql = "UPDATE order_detail"
//                + " SET order_detail_login = ?, order_detail_name = ?, order_detail_gender = ?, order_detail_password = ?, order_detail_role = ?"
//                + " WHERE order_detail_id = ?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, obj.getLogin());
//            stmt.setString(2, obj.getName());
//            stmt.setString(3, obj.getGender());
//            stmt.setString(4, obj.getPassword());
//            stmt.setInt(5, obj.getRole());
//            stmt.setInt(6, obj.getId());
////            System.out.println(stmt);
//            int ret = stmt.executeUpdate();
//            System.out.println(ret);
//            return obj;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//            return null;
//        }
        return null;
    }

    @Override
    public int delete(OrderDetail obj) {
        String sql = "DELETE FROM order_detail WHERE order_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}
